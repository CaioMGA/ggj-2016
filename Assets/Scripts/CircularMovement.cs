﻿using UnityEngine;
using System.Collections;

public class CircularMovement : MonoBehaviour  {

	public Transform center;
	public float degreesPerSecond = -85.0f;

	private Vector3 v;

	void Start() {
		v = transform.position - center.position;
	}

	void Update () {
		v = Quaternion.AngleAxis (degreesPerSecond * Time.deltaTime, Vector3.down) * v;
		transform.position = center.position + v;
	}
}