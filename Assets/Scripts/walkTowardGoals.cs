﻿using UnityEngine;
using System.Collections;

public class walkTowardGoals: MonoBehaviour {

	public Transform [] goals;
	public bool loop = false;
	public Vector3 origPoint;
	float distance;
	bool reached = false;
	int currentGoal = 0;

	public void Start()
	{
		origPoint = transform.position;
		transform.LookAt(goals[currentGoal]);
	}

	public void Update()
	{
		if(!reached)
		{
			distance = Vector3.Distance(transform.position, goals[currentGoal].transform.position);
			if(distance > .1)
			{
				move (transform.position, goals[currentGoal].transform.position);
			}
			else
			{
				reached = true;
			}
		}
		else
		{

			origPoint = transform.position;
			reached = false;
			if(currentGoal < goals.Length - 1){
				currentGoal++;
				origPoint = transform.position;
				transform.LookAt(goals[currentGoal]);
			} else if(loop) {
				currentGoal = 0;
				transform.LookAt(goals[currentGoal]);
			} 

		}
	}

	void move(Vector3 pos, Vector3 towards)
	{
		transform.position = Vector3.MoveTowards(pos, towards, .1f);
	}
}