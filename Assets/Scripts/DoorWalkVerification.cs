﻿using UnityEngine;
using System.Collections;

public class DoorWalkVerification : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		Debug.Log("Verificator OK");
		if(other.transform.CompareTag("Logic Door")){
			transform.GetComponentInParent<PlayerScript>().canWalkTheDoor = true;
		}
	}
}
