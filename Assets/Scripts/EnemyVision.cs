﻿using UnityEngine;
using System.Collections;

public class EnemyVision : MonoBehaviour {

	void OnTriggerStay(Collider other) {
		if(other.CompareTag("enemy")){

			Ray ray = new Ray(this.transform.position, other.transform.position);
			//Debug.DrawLine (transform.position, other.transform.position,Color.cyan,10);
			RaycastHit hit;
			if (Physics.Raycast (transform.position, transform.forward,out hit)) {
				Debug.DrawLine (this.transform.position, transform.forward,Color.cyan,10);
				if (!hit.collider.CompareTag ("wall")) {
					Debug.DrawLine (this.transform.position, hit.collider.transform.position,Color.red,10);
					other.GetComponent<RoboAss> ().crazy = true;
				}
			}		
		}
	}

}
