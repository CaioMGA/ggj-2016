﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public CameraScript cameraScript;
	public Transform nextRoomSpawn;
	public bool canWalkTheDoor = false;

	public bool killMe = false;

	CapsuleCollider collider;
	Rigidbody rb;

	void Awake(){
		collider = GetComponent<CapsuleCollider>();
		rb = GetComponent<Rigidbody>();
	}

	void OnTriggerEnter(Collider other){
		if(canWalkTheDoor){
			if(other.transform.CompareTag("Logic Door")){
				nextRoomSpawn = other.transform.GetChild(0);//get logic door
				int nextRoom = other.GetComponent<LogicDoorScript>().nextRoom;
				if(nextRoom == 0){
					cameraScript.loop();
					transform.position = nextRoomSpawn.position;
				} else {
					cameraScript.moveToRoom(nextRoom);
					transform.position = nextRoomSpawn.position;
					canWalkTheDoor = false;
				}

			}
		}

	}
	public void dieRoutine(){
		/*
		transform.position = new Vector3(
			transform.position.x,
			transform.position.y - 20,
			transform.position.z);
		rb.
		*/
	}

	/*
	void Update(){
		if(killMe){
			dieRoutine();
		}
	}
	*/
}