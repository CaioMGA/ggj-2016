﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menuScript : MonoBehaviour {

    public Button startText;
    public Button exitText;
    public Button settingsText;
    public Button creditsText;

	// Use this for initialization
	void Start () {
         //locates the Play and Exit buttons and attaches their buttons.
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        settingsText = settingsText.GetComponent<Button>();
        creditsText = creditsText.GetComponent<Button>();
	}
    
    public void StartLevel() {
        //Locates the first level of the game so it can be played.
        Application.LoadLevel(8);
    }

    public void SettingsScreen() {
        //Locates the Settings Screen level.
        Application.LoadLevel(2);
    }

    public void CreditsScreen() {
        //Locates the Credits Screen level.
        Application.LoadLevel(1);
    }

    public void ExitGame() {
        //Closes the game.
        Application.Quit();
    }

}
