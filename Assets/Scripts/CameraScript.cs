﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	public Transform [] rooms;
	public float speed;

	Vector3 destination;
	Vector3 position;
	public int currentRoom = 0;
	bool moving = false;

	public void moveToRoom(int roomNo){
		destination = rooms[roomNo].position;
		currentRoom = roomNo;
		position = transform.position;
		moving = true;
	}

	void FixedUpdate(){
		if(moving){
			position = Vector3.Lerp (position, destination, Time.deltaTime * speed);
			transform.position = position;
			if(position == destination){
				moving = false;

			}
		}
	}

	public void loop(){
		transform.position = rooms[4].position;
		moveToRoom(0);

	}
}
