﻿using UnityEngine;
using System.Collections;

public class PlayerMoveScript : MonoBehaviour {

	public float speed = 5f;

	float hAxis;
	float vAxis;
	int direction = 0;
	Vector3 rotationAxis;
	Rigidbody rb;
	/*
	 * 0 - up
	 * 1 - upright
	 * 2 - right
	 * 3 - downright
	 * 4 - down
	 * 5 - downleft
	 * 6 - left
	 * 7 - upleft
	 */ 

	void Awake(){
		rotationAxis = new Vector3(0, 1, 0);
		rb = GetComponent<Rigidbody>();
	}

	void getInputs(){
		hAxis = Input.GetAxisRaw("Horizontal");
		vAxis = Input.GetAxisRaw("Vertical");

		if(Input.GetKeyDown(KeyCode.Space)){
			action ();
		}
	}

	void move(){//
		rb.velocity = new Vector3(hAxis, 0, vAxis) * speed;
	}

	void action(){
		Debug.Log ("Action button pressed");
	}

	void solveDirection(){
		if(hAxis > 0){ // right
			if(vAxis > 0){
				//upright
				direction = 1;
			}
			if(vAxis < 0){
				//downright
				direction = 3;
			}
			if(vAxis == 0){
				//right
				direction = 2;
			}
		}

		if(hAxis < 0){ // left
			if(vAxis > 0){
				//downleft
				direction = 7;
			}
			if(vAxis < 0){
				//downright
				direction = 5;
			}
			if(vAxis == 0){
				//left
				direction = 6;
			}
		}

		if(hAxis == 0){ // null
			if(vAxis > 0){
				//up
				direction = 0;
			}
			if(vAxis < 0){
				//down
				direction = 4;
			}
		}
	}

	void solveRotation(){
		float rotationAngle = 0;
		switch(direction){
		case 0:
			rotationAngle = 0f;
			break;
		case 1:
			rotationAngle = 45f;
			break;
		case 2:
			rotationAngle = 90f;
			break;
		case 3:
			rotationAngle = 135f;
			break;
		case 4:
			rotationAngle = 180f;
			break;
		case 5:
			rotationAngle = 225f;
			break;
		case 6:
			rotationAngle = 270f;
			break;
		case 7:
			rotationAngle = 315f;
			break;
		}
		transform.rotation = Quaternion.AngleAxis(rotationAngle, rotationAxis);
	}

	void Update(){
		getInputs();
		solveDirection();
		solveRotation();
		move ();

	}
}
