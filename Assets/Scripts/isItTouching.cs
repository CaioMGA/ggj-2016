﻿using UnityEngine;
using System.Collections;

public class isItTouching : MonoBehaviour {

	bool touchingPlayer = false;

	bool doesIt(){
		return touchingPlayer;
	}

	void OnTriggerStay(Collider other){
		if(other.transform.CompareTag("Player")){
			touchingPlayer = true;
		}
	}

	void OnTriggerExit(Collider other){
		if(other.transform.CompareTag("Player")){
			touchingPlayer = false;
		}
	}
}
